# Dockerfile for Nooron User-Interface Testing

** This is currently a Work-in-Progress **


This Dockerfile is based on Buster (while the parent was based on Jessie):

It currently contains:

* build-essentials
* SCM tools
* Java 8
* Maven 3.3.9
* Node 8.x
* npm and yarn
* Google Chrome latest
* chromedriver
* Firefox ESR latest
* Bzip2 (for PhantomJS install)
* Zip

Nooron's testing needs are different from Atlassian's so ideally this Dockerfile
will have non-essentials removed:

* Java 8 (unless this is needed by selenium?)
* Maven
* PhantomJS dependencies


## How to build the image
```
docker build -t docker-node-chrome-firefox-build .
```

then use `docker images` to find the image ID.

With `docker run -it <IMAGE_ID>` you can test if your changes are the desired ones.

Then tag it: `docker tag <IMAGE_ID> <YOUR-USER>/docker-node-chrome-firefox-build:latest`

and finally publish it: `docker push <YOUR-USER>/docker-node-chrome-firefox-build`
